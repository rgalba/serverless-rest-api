# Serverless intro

# What is serverless computing

> Serverless computing is all about running your application code on small ingots of some CPU and memory without having to worry about the OS type, the software platform, or any of the underlying hardware either. Just take your code and run it! - [Mastering AWS Lambda] 

# Pros

- No infrastructure to manage
- Runs only needed
- Autoscaling built in
- Microservices compatible
- Event-driven applications

# Cons

- Stateless
- Vendor lock-in
- Lack of tools
- Fully cloud-native approach (depends on another services)
- Complexity

# Introducing Serverless framework

## Core concepts

### Functions

>A Function is an AWS Lambda function. It's an independent unit of deployment, like a microservice. It's merely code, deployed in the cloud.

. Saving a user to the database
. Processing a file in a database
. Performing a scheduled task

### Events

>Anything that triggers an AWS Lambda Function to execute is regarded by the Framework as an Event.

. An AWS API Gateway HTTP endpoint request (e.g., for a REST API)
. An AWS S3 bucket upload (e.g., for an image)
. A CloudWatch timer (e.g., run every 5 minutes)

### Resources

>Resources are AWS infrastructure components which your Functions use such as:

. An AWS DynamoDB Table (e.g., for saving Users/Posts/Comments data)
. An AWS S3 Bucket (e.g., for saving images or files)

### Services

>A Service is the Framework's unit of organization. You can think of it as a project file, though you can have multiple services for a single application. It's where you define your Functions, the Events that trigger them, and the Resources your Functions use, all in one file entitled serverless.yml.

When you deploy with the Framework by running serverless deploy, everything in serverless.yml is deployed at once.

### Plugins

>You can overwrite or extend the functionality of the Framework using Plugins. Every serverless.yml can contain a plugins: property, which features multiple plugins.

## Getting start with serverless

### Installing the serverless cli

```bash
npm install -g serverless
# Updating serverless from a previous version of serverless
npm install -g serverless
# Login to the serverless platform (optional)
serverless login
```

## Hands on

Building a NodeJS REST API

### Create and deploy a single endpoint

- Create and install packages:
```sh
mkdir my-express-application && cd my-express-application
npm init -f
npm install --save express serverless-http
```

The serverless-http package is a handy piece of middleware that handles the interface between your Node.js application and the specifics of API Gateway.

- Create `index.js` file with first endpoint
- Create `serverless.yml` for the first deploy
- Adding a DynamoDB table with REST-like endpoints
- Local development configuration with Serverless offline plugin

# Get user

```sh
curl --request GET \
  --url https://5tehqkvj1j.execute-api.us-east-1.amazonaws.com/dev/users/john_doe
```

# Create user
```sh
curl --request POST \
  --url https://5tehqkvj1j.execute-api.us-east-1.amazonaws.com/dev/users \
  --header 'content-type: application/json' \
  --data '{
	"userId":"john_doe",
	"name":"John Doe"
}'
```

### References
[Serverless framework][https://serverless.com]
[Core concepts][https://serverless.com/framework/docs/providers/aws/guide/intro/]
[NodeJS REST API][https://serverless.com/blog/serverless-express-rest-api/]
[Mastering AWS Lambda][https://www.packtpub.com/virtualization-and-cloud/mastering-aws-lambda]
